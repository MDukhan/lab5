# Questions for Lab 5

## Part 0: Getting started

* *What is the name of the processor which you used for this assignment?*

Part 1: Profiling
---------------------

* *What is the IPC of the utility in our use case? Is it good?*
* *What is the fraction of mispredicted branches? Is it acceptable?*
* *What is the rate of cache misses? How do you feel about this number?*

* *Which two functions take most of the execution time? What do they do?*

Part 2: Compiler Optimizations
------------------------------

* *What is the "User Time" for program execution before you start optimizing?*

* *What is the "User Time" for program execution after you completed **all** three steps and rune the program with `-fprofile-use`*?
